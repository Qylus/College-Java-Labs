public class Vet
{
	private String name;
	
	public Vet(String name)
	{ 
		this.name = name;
	}
	
	public void Vaccinate(Animal a)
	{
		System.out.print("\n\t" + name + " is vaccinating.");
		System.out.println("\n\t" + a.species() + " has been vaccinated.\n\t" + a.toString());	
	}
}