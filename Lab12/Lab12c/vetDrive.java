class vetDrive
{
	public static void main (String[] args)
	{
		Vet vet = new Vet("Johnny Joestar's Animal Kingdom");
		Dog dog1 = new Dog("Malamute", 7, 'F');
		Cat cat1 = new Cat("Russian Blue", 5, 'M');
		vet.Vaccinate(dog1);
		vet.Vaccinate(cat1);
	}	
}