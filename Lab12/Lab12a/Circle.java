class Circle extends Point
{
	int radius;
	
	public Circle(int x, int y, int radius)
	{
		super(x, y);
		this.radius = radius;
	}
	
	public void setRadius(int c)
	{
		radius = c;
	}
	
	public int getRadius()
	{
		return radius;
	}
	
	public String toString()
	{
		String s = "X coordinate : " + x + "\nY coordinate : " + y + "\nRadius : " + radius;
		
		return s;
	}
}