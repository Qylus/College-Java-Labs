import java.util.Scanner;

class Point
{
	int x;
	int y;
	
	protected Point(int x, int y)
	{
		this.x = x;
		this.y = y;
	}
	
	public void setX(int a)
	{
		x = a;
	}
	
	public int getX()
	{
		return x;
	}
	
	public void setY(int b)
	{
		y = b;
	}
	
	public int getY()
	{
		return y;
	}
	
	public String toString()
	{
		String s = "X coordinate : " + x + "\nY coordinate : " + y;
		
		return s;
	}
	
}
