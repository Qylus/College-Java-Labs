// Author 			: Oisin Cawley
// Date 			: Feb-2016
// Purpose 			: A superclass Person

public class Person {
   // Instance variables
    String name;
    String address;
   
   // Constructor
   public Person(String name, String address) {
      this.name = name;
      this.address = address;
   }
   
   // Getters
   public String getName() {
      return name;
   }
   public String getAddress() {
      return address;
   }
   
   public String toString() {
      return name + "(" + address + ")";
   }
}