import java.util.Scanner;

class lab4a
{
	public static void main(String[] args)
	{
		String firstname = "";
		String lastname = "";
		String fullname;
		int length1;
		int length2;
		int i;
		int numA = 0;
		boolean isA = false;
		
		Scanner keyboard = new Scanner (System.in);
		
		System.out.print("Please input first name : ");
		firstname = keyboard.nextLine();
		length1 = firstname.length();
		System.out.print("Please input last name : ");
		lastname = keyboard.nextLine();
		length2 = lastname.length();
		fullname = firstname + " " + lastname;
		System.out.println("\n" + firstname + " is "+ length1 + " characters long, and " + lastname + " is "+ length2 + " characters long.");
		System.out.print("The initials are " + firstname.charAt(0) + " " + lastname.charAt(0));
		System.out.println("\nThe last characters in each name are " + firstname.charAt(length1 - 1) + " and " + lastname.charAt(length2 - 1));
		System.out.print("The full name in upper case is " + fullname.toUpperCase());
		
		for (i=0;i <= firstname.length() - 1; i++)
			{
				if (firstname.charAt(i) == 'a')
					{
						isA = true;
						
						if (isA == true)
							{
								numA = i;
							}
					}
			}
		
		if (isA == true)
			System.out.println("\nThe first name containted an 'a' on index position " + numA);
		
		
		else
			System.out.println("\nThe first name did not contain an 'a'");


	}
}