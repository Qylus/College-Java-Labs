import java.util.Scanner;

class lab4b2
{
	public static void main (String[] args)
	{
		double chipProf;
		int chipsell;
		double burgProf;
		int burgsell;
		
		Scanner keyboard = new Scanner (System.in);
		
		System.out.print("Please enter the number of chips sold : ");
		chipsell = keyboard.nextInt();
		System.out.print("Please enter the number of burgers sold : ");
		burgsell = keyboard.nextInt();
		
		chipProf = chipsell * 0.35;
		burgProf = burgsell * 0.55;
	
		System.out.println("Profit from chips is " + chipProf + " euro");
		System.out.println("Profit from burgers is " + burgProf + " euro");
		System.out.println("Total profit = " + (chipProf + burgProf) + " euro");

		
	}
}