// a program to print a daily schedule with the 
// amount of time spent on each task.
public class lab4b1
{
public static void main (String args[])
 {
final int DRESS = 45;
final int EAT = 30;
final int DRIVE = 30;
final int CLASS = 60;
int totalmins = 0;totalmins = DRESS + 3*EAT + 2*DRIVE + 4*CLASS;
System.out.println("You spend " + totalmins/60 );
System.out.println("hours and " + totalmins % 60 );
System.out.println("minutes a day on scheduled activities.");
}
} 

/*
a.	What is written by program Schedule (What does it do)? 
b.	List the identifiers that are defined in program Schedule. 
c.	Which of these identifiers are named constants?
d.	What is the purpose of the % in the program?

a.	Lists number of hours and minutes spent on scheduled activities
b.	DRESS, EAT, DRIVE, CLASS, SCHEDULE, totalmins
c.	DRESS, EAT, DRIVE, CLASS
d.	It functions to determine the remainer of the compared values

*/
