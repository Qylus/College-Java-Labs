public class Address
{

	String street, city, county;
	
	public Address()			// constructor method #1
	{
		street = "";
		city = "";
		county = "";
	}

	public void setStreet(String x)
	{
		street = x;
	}
	
	public String getStreet()
	{
		return street;
	}
	
	public void setCity(String y)
	{
		city = y;
	}
	
	public String getCity()
	{
		return city;
	}
	
	public void setCounty(String z)
	{
		county = z;
	}
	
	public String getCounty()
	{
		return county;
	}
	
	
	public String toString()
	{
		String s = "Street: " + street + "\n\tCity: " + city + "\n\tCounty: " + county + "\n\n";
		
		return s;
	}
	
		
}