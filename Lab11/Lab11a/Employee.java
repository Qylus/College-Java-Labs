public class Employee
{

	static int empNum = 999;
	int offnum, counter;
	String stat = "";
	String cartype;
	
	public Employee()			// constructor method #1
	{
		empNum++;
		counter = empNum;
		stat = "";
		cartype = "";
		offnum = 0;
	}

	public String toString()
	{
		String s = "\n\tEmployee ID: " + counter + "\n\tOffice Number: " + offnum + "\n";
		
		if (stat == "Staff")
			s = s + "\tType: Staff";
			
		else
			s = s + "\tType: Manager\n\tCar : " + cartype;
		
		return s;
	}
	
	public void setStatus(String x)
	{
		stat = x;

	}
	
	public String getStatus()
	{
		return stat;
	}
	
	public void setCar(String y)
	{
		cartype = y;
	}
	
	public String getCar()
	{
		return cartype;
	}	
	
	public void setOffice(int z)
	{
		offnum = z;
	}	
	
	public int getOffice()
	{
		return offnum;
	}
}