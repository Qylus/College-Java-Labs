public class Office
{

	static int roomNum = 99;
	int counter = 99;
	int slots = 2;
	
	private Employee[] empArray;
	
	public Office()			// constructor method #1
	{
		roomNum++;
		counter = roomNum;
		empArray = new Employee[2];
	}

	public String toString()
	{
		String s = "\n\tOffice " + counter + "\n\tAvailable slots: " + slots + "\n\n";
		
		return s;
	}
	
	public void addEmployee(Employee x)
	{	
		if (slots < 1)
		{
			System.out.println("\n\n\tMaximum number of employees already assigned to office. \n\n\t");
			slots = 1;
		}
		
		slots--;
	}
		
}