import java.util.*;

class myHR
{
	public static void main (String[] args) 
	{
		Office offA = new Office();
		Office offB = new Office();
		Office offC = new Office();
		
		Employee emp1 = new Employee();
		Employee emp2 = new Employee();
		Employee emp3 = new Employee();
		Employee emp4 = new Employee();
		Employee emp5 = new Employee();
		
		Address add1 = new Address();
		Address add2 = new Address();
		Address add3 = new Address();
		Address add4 = new Address();
		Address add5 = new Address();

				
		int input, countA = 0, offNum;
		String hold;
		
		Scanner s = new Scanner(System.in);
		
		do
		{
			System.out.println("\n\t(Please input the corresponding number)\n\n\t1. List All Offices\n\t2. Create New Employee Record\n\t3. List All Employees\n\t4. Exit");
			
			input = s.nextInt();
			s.nextLine();
			
			if (input == 1)
			{
				System.out.print(offA);
				System.out.print(offB);
				System.out.print(offC);
			}
			
			else if (input == 2)
			{
				if(countA == 0)
				{	
					System.out.print("\n\tEmployee Address\n\n\tStreet : ");
						add1.setStreet(s.nextLine());
					System.out.print("\n\tCity : ");
						add1.setCity(s.nextLine());
					System.out.print("\n\tCounty : ");
						add1.setCounty(s.nextLine());
						
					do
					{
						System.out.print("\n\tIs the employee staff or manager? : ");
						hold = s.nextLine();
						
						if (hold.equalsIgnoreCase("staff"))
							{
								emp1.setStatus("Staff");
								break;
							}
							
							
						else if (hold.equalsIgnoreCase("manager"))
							{
								emp1.setStatus("Manager");
							
								System.out.print("\n\tCar type : ");
								emp1.setCar(s.nextLine());
								break;
							}
							
						else 
							System.out.println("\n\tInvalid. Try again.\t");
					}
					while(!hold.equalsIgnoreCase("staff") || !hold.equalsIgnoreCase("manager") );
							

					do
					{	
						
						System.out.print("\n\tWhich office would you like to add this employee to? ");
						offNum = s.nextInt();
						emp1.setOffice(offNum);
						
						if(offNum == 100)
							offA.addEmployee(emp1);
								
						else if (offNum == 101)
							offB.addEmployee(emp1);
							
						else if (offNum == 102)
							offC.addEmployee(emp1);
						
						else
							System.out.println("\n\tNo such office exists. Try again.");
					}
					
					while (offNum < 100 || offNum > 102);
					
				}
				
				else if(countA == 1)
				{
					
					System.out.print("\n\tEmployee Address\n\n\tStreet : ");
						add2.setStreet(s.nextLine());
					System.out.print("\n\tCity : ");
						add2.setCity(s.nextLine());
					System.out.print("\n\tCounty : ");
						add2.setCounty(s.nextLine());
						
						do
						{
							System.out.print("\n\tIs the employee staff or manager? : ");
							hold = s.nextLine();
							
							if (hold.equalsIgnoreCase("staff"))
							{
								emp2.setStatus("Staff");
								break;
							}
							
							else if (hold.equalsIgnoreCase("manager"))
							{
								emp2.setStatus("Manager");
							
								System.out.print("\n\tCar type : ");
								emp2.setCar(s.nextLine());
								break;
							}
							
							else 
								System.out.println("\n\tInvalid. Try again.\t");
						}
						while(!hold.equalsIgnoreCase("staff") || !hold.equalsIgnoreCase("manager") );
						
					do
					{	
						System.out.print("\n\tWhich office would you like to add this employee to? ");
						offNum = s.nextInt();
						emp2.setOffice(offNum);
						
						if(offNum == 100)
							offA.addEmployee(emp2);
							
						else if (offNum == 101)
							offB.addEmployee(emp2);
							
						else if (offNum == 102)
							offC.addEmployee(emp2);
						
						else
							System.out.println("\n\tNo such office exists. Try again.");
					}
					
					while (offNum < 100 || offNum > 102);
					
				}
				
				else if(countA == 2)
				{
					
					System.out.print("\n\tEmployee Address\n\n\tStreet : ");
						add3.setStreet(s.nextLine());
					System.out.print("\n\tCity : ");
						add3.setCity(s.nextLine());
					System.out.print("\n\tCounty : ");
						add3.setCounty(s.nextLine());
						
					do
					{
						System.out.print("\n\tIs the employee staff or manager? : ");
						hold = s.nextLine();
						
						if (hold.equalsIgnoreCase("staff"))
							{
								emp3.setStatus("Staff");
								break;
							}
							
							
						else if (hold.equalsIgnoreCase("manager"))
							{
								emp3.setStatus("Manager");
							
								System.out.print("\n\tCar type : ");
								emp3.setCar(s.nextLine());
								break;
							}
							
						else 
							System.out.println("\n\tInvalid. Try again.\t");
					}
					while(!hold.equalsIgnoreCase("staff") || !hold.equalsIgnoreCase("manager") );
							

					do
					{	
						System.out.print("\n\tWhich office would you like to add this employee to? ");
						offNum = s.nextInt();
						emp3.setOffice(offNum);
						
						if(offNum == 100)
							offA.addEmployee(emp3);
							
						else if (offNum == 101)
							offB.addEmployee(emp3);
							
						else if (offNum == 102)
							offC.addEmployee(emp3);
						
						else
							System.out.println("\n\tNo such office exists. Try again.");
					}
					
					while (offNum < 100 || offNum > 102);
				}
				
				else if(countA == 3)
				{
					
					System.out.print("\n\tEmployee Address\n\n\tStreet : ");
						add4.setStreet(s.nextLine());
					System.out.print("\n\tCity : ");
						add4.setCity(s.nextLine());
					System.out.print("\n\tCounty : ");
						add4.setCounty(s.nextLine());
						
					do
					{
						System.out.print("\n\tIs the employee staff or manager? : ");
						hold = s.nextLine();
						
						if (hold.equalsIgnoreCase("staff"))
							{
								emp4.setStatus("Staff");
								break;
							}
							
							
						else if (hold.equalsIgnoreCase("manager"))
							{
								emp4.setStatus("Manager");
							
								System.out.print("\n\tCar type : ");
								emp4.setCar(s.nextLine());
								break;
							}
							
						else 
							System.out.println("\n\tInvalid. Try again.\t");
					}
					while(!hold.equalsIgnoreCase("staff") || !hold.equalsIgnoreCase("manager") );
							
					do
					{	
						System.out.print("\n\tWhich office would you like to add this employee to? ");
						offNum = s.nextInt();
						emp4.setOffice(offNum);
						
						if(offNum == 100)
							offA.addEmployee(emp4);
							
						else if (offNum == 101)
							offB.addEmployee(emp4);
							
						else if (offNum == 102)
							offC.addEmployee(emp4);
						
						else
							System.out.println("\n\tNo such office exists. Try again.");
					}
					
					while (offNum < 100 || offNum > 102);
				}
				
				else if(countA == 4)
				{
					
					System.out.print("\n\tEmployee Address\n\n\tStreet : ");
						add5.setStreet(s.nextLine());
					System.out.print("\n\tCity : ");
						add5.setCity(s.nextLine());
					System.out.print("\n\tCounty : ");
						add5.setCounty(s.nextLine());
						
					do
					{
						System.out.print("\n\tIs the employee staff or manager? : ");
						hold = s.nextLine();
						
						if (hold.equalsIgnoreCase("staff"))
							{
								emp5.setStatus("Staff");
								break;
							}
							
							
						else if (hold.equalsIgnoreCase("manager"))
							{
								emp5.setStatus("Manager");
							
								System.out.print("\n\tCar type : ");
								emp5.setCar(s.nextLine());
								break;
							}
							
						else 
							System.out.println("\n\tInvalid. Try again.\t");
					}
					while(!hold.equalsIgnoreCase("staff") || !hold.equalsIgnoreCase("manager") );
							
						
					do
					{	
						System.out.print("\n\tWhich office would you like to add this employee to? ");
						offNum = s.nextInt();
						emp5.setOffice(offNum);
						
						if(offNum == 100)
							offA.addEmployee(emp5);
							
						else if (offNum == 101)
							offB.addEmployee(emp5);
							
						else if (offNum == 102)
							offC.addEmployee(emp5);
						
						else
							System.out.println("\n\tNo such office exists. Try again.");
					}
					
					while (offNum < 100 || offNum > 102);
				}
				
				else
					System.out.println("\n\tYou have added the maximum number of employees.");
					
				countA++;
			} 
			
			else if (input == 3)
			{
				if(countA == 0)
					System.out.print("\n\tNo employee records found.\n\n");
				
				else if(countA == 1)
					System.out.print(emp1 + "\n\t" + add1);
				
				else if(countA == 2)
				{
					System.out.print(emp1 + "\n\t" + add1);
					System.out.print(emp2 + "\n\t" + add2);
				}
				
				else if(countA == 3)
				{
					System.out.print(emp1 + "\n\t" + add1);
					System.out.print(emp2 + "\n\t" + add2);
					System.out.print(emp3 + "\n\t" + add3);
				}
				
				else if(countA == 4)
				{
					System.out.print(emp1 + "\n\t" + add1);
					System.out.print(emp2 + "\n\t" + add2);
					System.out.print(emp3 + "\n\t" + add3);
					System.out.print(emp4 + "\n\t" + add4);
				}
				
				else if(countA >= 5)
				{
					System.out.print(emp1 + "\n\t" + add1);
					System.out.print(emp2 + "\n\t" + add2);
					System.out.print(emp3 + "\n\t" + add3);
					System.out.print(emp4 + "\n\t" + add4);
					System.out.print(emp5 + "\n\t" + add5);
				}
			}
		}
		while (input != 4);
		
		s.close();
	}
}