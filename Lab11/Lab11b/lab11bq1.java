import java.util.Scanner;

class lab11q1
{
	public static void main (String[] args)
	{
		Scanner s = new Scanner(System.in);
		
		final int[] myArray = new int[5];
		int i, Oc = 0, Ec = 0;
		
		for (i = 0; i <= myArray.length - 1; i++)
			{
				System.out.print("\n\tPlease enter number " + (i+1) + ": ");
				myArray[i] = s.nextInt();
			}
		
		for (i = 0; i <= myArray.length - 1; i++)
			{
				if (isEven(myArray[i]) == true)
					Ec++;
					
				else
					Oc++;		
			}
			
		System.out.print("\n\tNumber of odd numbers : " + Oc + "\n\tNumber of even numbers : " + Ec + "\n\n\t");
	}
	
	public static boolean isEven(int x)
		{
			if (x % 2 == 0)
				return true;
				
			else
				return false;
		}
}