import java.util.Scanner;

class lab11q2
{
	public static void main (String[] args)
	{
		Scanner s = new Scanner(System.in);
		
		int i, arrLength, x = 0;
		
		System.out.print("\n\tHow long would you like your array to be? : ");
		arrLength = s.nextInt();
		
		final int[] myArray = new int[arrLength];
		
		for (i = 0; i <= myArray.length - 1; i++)
				{
					System.out.print("\n\tPlease enter number " + (i+1) + ": ");
					myArray[i] = s.nextInt();
				}
		
			
		System.out.print("\n\tHighest number : " + findHighest(myArray) + "\n\tLowest number : "  + findLowest(myArray) + "\n\n\t");
	}
	
	
	public static int findHighest(int[] myArray)
		{
			int i, highNum = myArray[0];
			
			for (i = 1; i <= myArray.length - 1; i++)
				{
					
					if (myArray[i] > highNum)
						highNum = myArray [i];	
				}
				
			return highNum;
				
		}	
		
		
	public static int findLowest(int[] myArray)
		{
			int i, lowNum = myArray[0];
			
			for (i = 1; i <= myArray.length - 1; i++)
				{
					if (myArray[i] < lowNum)
						lowNum = myArray [i];	
				}
				
			return lowNum;		
		}
}