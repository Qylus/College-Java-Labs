import java.util.Scanner;

class lab11q3
{
	public static void main (String[] args)
	{
		Scanner s = new Scanner(System.in);
		
		int i, arrLength, x = 0;
		
		System.out.print("\n\tHow long would you like your array to be? : ");
		arrLength = s.nextInt();
		
		final int[] myArray = new int[arrLength];
		
		for (i = 0; i <= myArray.length - 1; i++)
				{
					System.out.print("\n\tPlease enter number " + (i+1) + ": ");
					myArray[i] = s.nextInt();
				}
		
			
		System.out.print("\n\t" + findHighest(myArray) + "\n\t"  + findLowest(myArray) + "\n\n\t");
	}
	
	
	public static String findHighest(int[] myArray)
		{
			int i, highNum = myArray[0], count = 0;
			String s;
			
			for (i = 1; i <= myArray.length - 1; i++)
				{
					
					if (myArray[i] > highNum)
						highNum = myArray [i];	
				}
				
			for (i = 0; i <= myArray.length - 1; i++)
				{
					if (myArray[i] == highNum)
						count++;	
				}
				
			s = "The highest number, " + highNum + ", appeared " + count + " times.";
				
			return s;
				
		}	
		
		
	public static String findLowest(int[] myArray)
		{
			int i, lowNum = myArray[0], count = 0;
			String s;
						
			for (i = 1; i <= myArray.length - 1; i++)
				{
					if (myArray[i] < lowNum)
						lowNum = myArray [i];	
				}
				
			for (i = 0; i <= myArray.length - 1; i++)
				{
					if (myArray[i] == lowNum)
						count++;	
				}
				
			s = "The lowest number, " + lowNum + ", appeared " + count + " times.";
			
			return s;		
		}
}