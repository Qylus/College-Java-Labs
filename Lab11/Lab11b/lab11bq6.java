import java.util.Scanner;

class lab11q6
{
	public static void main (String[] args)
	{
		Scanner s = new Scanner(System.in);
		
		int i, i2;
		String x;
		boolean palindrome;
		
		System.out.println("\n\tHow many strings would you like to enter? : ");
		String[] myArray = new String[s.nextInt()];
		s.nextLine(); //clears stream
		
		for (i = 0; i <= myArray.length - 1; i++)
			{
				System.out.print("\n\tEnter String number " + (i + 1) + " : ");
				myArray[i] = s.nextLine();
			}
		
		s.close();
		
		for (i = 0; i < myArray.length; i++)
			{
				palindrome = true;
				
				x = myArray[i].toLowerCase();
				
				for (i2 = 0; i2 <= x.length() - 1; i2++)
					if (x.charAt(i2) != x.charAt(x.length() - 1 - i2))
						palindrome = false;
					
				if (palindrome == false)
					System.out.print("\n\t" + myArray[i] + " is not a palindrome.\n");
					
				else
					System.out.print("\n\t" + myArray[i] + " is a palindrome.\n");
			}
			
		System.out.print("\n\t");				
	}
	
}
