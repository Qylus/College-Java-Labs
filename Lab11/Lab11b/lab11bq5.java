import java.util.Scanner;

class lab11q5
{
	public static void main (String[] args)
	{
		Scanner s = new Scanner(System.in);
		
		System.out.println("\n\tEnter a palindrome (No spaces)");
		char[] myArray = s.next().toCharArray(); 
		
		s.close();
		
		int i;
		boolean palindrome = true;
				
		for (i = 0; i <= myArray.length - 1; i++)	
			if (myArray[i] != myArray[myArray.length - 1 - i])
				palindrome = false;		
			
		if (palindrome == true)
			System.out.print("\n\tInput is a palindrome.\n\t");
			
		else
			System.out.print("\n\tInput is not a palindrome.\n\t");
	}
	
}
