public class lab9b2
{
	public static void main (String[] args)
	{
		Rectangle rec1 = new Rectangle();
		
		int length = 20, width = 30;

		rec1.setWidth(width);
		rec1.setLength(length);
		System.out.println(rec1.toString() + " : Area is " + rec1.getArea() + " : Perimeter is " + rec1.getPerimeter());

	}
}