import java.util.Scanner;

public class lab9a1
{
	public static void main (String[] args)
	{
		HotelRoom roomA = new HotelRoom();
		HotelRoom roomB = new HotelRoom();

		roomA.setRoom(200);
		roomA.setRoomT("Single");
		System.out.println("Room A : Room number is " + roomA.getRoom() + ". Room type is " + roomA.getRoomT());
		
		roomB.setRoom(201);
		roomB.setRoomT("Double");
		System.out.println("Room B : Room number is " + roomB.getRoom() + ". Room type is " + roomB.getRoomT());

	}
}