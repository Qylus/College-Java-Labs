import java.util.Scanner;

public class HotelRoom
{
	private String roomType;
    private int roomNumber;
    private int isVacant;
    private double rate;
    private boolean occupied;
	
	public HotelRoom()			// constructor method #1
	{
		setRoom(0);
		setVacant(0);
		setRate(0);
		roomType = "";		
	}
		
	public HotelRoom(int roomNum, int vac, double rate, String type)	// constructor method #2
	{
		setRoom(202);
		setVacant(0);
		setRate(90);
		roomType = "Single";
	}
	
	public boolean isOccupied(int vac)
	{
		if (getVacant() == 0)
			return false;
			
		else
			return true;
	}
	
	
	public void setRoom(int x)
	{
		roomNumber = x;		
	}
	
	public int getRoom()
	{
		return roomNumber;
	}
	
	public void setVacant (int vac)
	{
		isVacant = vac;
	}

	public int getVacant()
	{
		return isVacant;
	}
	
	public void setRate (double bgu)
	{
		rate = bgu;
	}

	public double getRate()
	{
		return rate;
	}
	
	public void setRoomT(String aaah)
	{
		roomType = aaah;		
	}

	public String getRoomT()
	{
		return roomType;
	}
}