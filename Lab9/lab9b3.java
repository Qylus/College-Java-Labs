import java.util.Scanner;
public class lab9b3
{
	public static void main (String[] args)
	{
		Scanner s = new Scanner(System.in);
		Rectangle rec1 = new Rectangle();
		
		int length, width;
		
		System.out.print("Enter length : ");
		length = s.nextInt();
		System.out.print("Enter width : ");
		width = s.nextInt();

		rec1.setWidth(width);
		rec1.setLength(length);
		

		System.out.println(rec1.toString() + " : Area is " + rec1.getArea() + " : Perimeter is " + rec1.getPerimeter());
		
		rec1.printRectangle();
		
		s.close();

	}
}