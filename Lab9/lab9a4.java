public class lab9a4
{
	public static void main (String[] args)
	{
		HotelRoom roomA = new HotelRoom();
		HotelRoom roomB = new HotelRoom();
		HotelRoom roomC = new HotelRoom(202, 0, 90, "Single");
		
		roomA.setVacant(1);
		roomA.setRoom(200);
		roomA.setRoomT("Single");
		roomA.setRate(100);

		System.out.println("\n\tRoom A : Room number is " + roomA.getRoom() + ". Room type is " + roomA.getRoomT());		
		System.out.print((roomA.getVacant() == 1) ? "\tRoom is occupied. " : "\tRoom is unoccupied. ");			
		System.out.print("Nightly rate is " + roomA.getRate() + " euros.");
		System.out.print(roomA.isOccupied(roomA.getVacant()) == true ? "\n\tRoom confirmed occupied. " : "\n\tRoom confirmed unoccupied. ");

	
		
		roomB.setVacant(0);
		roomB.setRoom(201);
		roomB.setRoomT("Double");
		roomB.setRate(80);

		System.out.println("\n\n\tRoom B : Room number is " + roomB.getRoom() + ". Room type is " + roomB.getRoomT());		
		System.out.print((roomB.getVacant() == 1) ? "\tRoom is occupied. " : "\tRoom is unoccupied. ");			
		System.out.print("Nightly rate is " + roomB.getRate() + " euros.");		
		System.out.print(roomA.isOccupied(roomB.getVacant()) == true ? "\n\tRoom confirmed occupied. " : "\n\tRoom confirmed unoccupied. ");

		
		
		
		System.out.println("\n\n\tRoom C : Room number is " + roomC.getRoom() + ". Room type is " + roomC.getRoomT());		
		System.out.print(roomC.getVacant() == 1 ? "\tRoom is occupied. " : "\tRoom is unoccupied. ");
		System.out.print("Nightly rate is " + roomC.getRate() + " euros.");		
		System.out.print(roomC.isOccupied(roomC.getVacant()) == true ? "\n\tRoom confirmed occupied. " : "\n\tRoom confirmed unoccupied. ");

		
		
		
		
		System.out.println("\n\t");

	}
}