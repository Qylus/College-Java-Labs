import java.util.Scanner;

public class Rectangle
{
    private int length;
    private int width;
    private int area;
    private int perimeter;
    private String print;
	
	public Rectangle()			// constructor method #1
	{
		setLength(1);
		setWidth(1);
		perimeter = 1;
		print = "";
	
	}
		
	/*public Rectangle()	// constructor method #2
	{
		
	} */
	
	public void setLength(int x)
	{
		if (x > 0 && x < 40)
			length = x;	
			
		else
			length = 40;
				
	}
	
	public int getLength()
	{
		return length;
	}
	
	public void setWidth (int y)
	{
		if (y > 0 && y < 40)
			width = y;	
			
		else
			width = 40;
	}

	public int getWidth()
	{
		return width;
	}
	
	public int getArea()
	{
		area = length * width;
		return area;
	}
	
	public int getPerimeter()
	{
		perimeter = (length + width) * 2;
		return perimeter;
	}
	
	public String toString()
	{
		return "Width = " + length + ", Length = " + width;
	}
	
	public void printRectangle()
	{
		int i, space;
		
		for(i = 0; i < length; i++) 
			System.out.print("*"); // Prints top line of rectangle
			        
		System.out.print("\n*");
			        	
        for (i = 0; i < width - 1; i++)
        {
	        for (space = 0; space < length - 2 ; space++) 
		        System.out.print(" ");
			    	
	        System.out.print("*\n*");		
        }
        
        for( i = 0; i < length - 1; i++) 
        	System.out.print("*");
        	
        System.out.println();
        


	}
	
}