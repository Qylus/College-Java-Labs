public class lab9a2
{
	public static void main (String[] args)
	{
		HotelRoom roomA = new HotelRoom();
		HotelRoom roomB = new HotelRoom();
		
		roomA.setVacant(1);
		roomA.setRoom(200);
		roomA.setRoomT("Single");
		roomA.setRate(100);

		System.out.println("\n\tRoom A : Room number is " + roomA.getRoom() + ". Room type is " + roomA.getRoomT());
		
		if (roomA.getVacant() == 1)
			System.out.print("\tRoom is occupied. ");
		else
			System.out.print("\tRoom is unoccupied. ");
			
		System.out.print("Nightly rate is " + roomA.getRate() + " euros.");

	
		
		roomB.setVacant(0);
		roomB.setRoom(201);
		roomB.setRoomT("Double");
		roomB.setRate(80);

		System.out.println("\n\n\tRoom B : Room number is " + roomB.getRoom() + ". Room type is " + roomB.getRoomT());
		
		if (roomB.getVacant() == 1)
			System.out.print("\tRoom is occupied. ");
		else
			System.out.print("\tRoom is unoccupied. ");
			
		System.out.print("Nightly rate is " + roomB.getRate() + " euros.");
		
		
		
		
		System.out.println("\n");

	}
}