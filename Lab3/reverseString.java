import java.util.Scanner;

class reverseString

{
	public static void main (String[] args)
	
	{
		String input = "";
		int i;
		Scanner keyboard = new Scanner(System.in);
		
		System.out.print("Please enter your string : ");
		input =	keyboard.nextLine(); // input get
		
		for (i = input.length() -1; i >= 0 ; i--) //loop runs from the index of the strings last character, to its first
				System.out.print(input.charAt(i)); //prints out each character as it goes, resulting in the reverse of the string being printed
			
		System.out.println();
			

	}
}