public class BankCustomer
{
	private String name, address;
	private int count = 0;
	
	private SavingsAccount[] accArray;
	
	public BankCustomer(String name, String address)			// constructor method #1
	{
		name = "";
		address = "";
		accArray = new SavingsAccount[3];
	}

	
	public String getName()
	{
		return name;
	}
	
	public void setName(String y)
	{
		name = y;
	}
	
	public String getAddress()
	{
		return address;
	}

	public void setAddress(String z)
	{
		address = z;
	}
	
	public void addAccount(SavingsAccount x)
	{
	
		System.out.print(x.toString());
	}
	
	public double balance(SavingsAccount x)
	{
		return x.getBalance();
	}
	
	public String summary(SavingsAccount x)
	{
		String ret = "ID : " + x.getID() + "\n\tBalance : " + x.getBalance() + "\n\n\t";
		
		return ret;
	}
		
	
	
		
}