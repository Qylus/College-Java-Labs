public class lab10q2
{
	public static void main (String[] args)
	{		
		SavingsAccount saver1 = new SavingsAccount();
		SavingsAccount saver2 = new SavingsAccount();
		
		saver1.modifyInterestRate(0.04);
		saver2.modifyInterestRate(0.04);
		
		saver1.setBalance(2000);
		System.out.println("\n\tID : " + saver1.getID() + "\n\tCurrent Balance : " + saver1.getBalance());
		System.out.println("\tMonthly Interest : " + saver1.calculateMonthlyInterest());
		System.out.println("\tNext months balance : " + saver1.savingsBalance());
		
		saver2.setBalance(3000);
		System.out.println("\n\tID : " + saver2.getID() + "\n\tCurrent Balance : " + saver2.getBalance());
		System.out.println("\tMonthly Interest : " + saver2.calculateMonthlyInterest());
		System.out.println("\tNext months balance : " + saver2.savingsBalance());
		
		saver1.modifyInterestRate(0.05);
		saver2.modifyInterestRate(0.05);
		
		saver1.setBalance(2000);
		System.out.println("\n\tID : " + saver1.getID() + "\n\tCurrent Balance : " + saver1.getBalance());
		System.out.println("\tMonthly Interest : " + saver1.calculateMonthlyInterest());
		System.out.println("\tNext months balance : " + saver1.savingsBalance());
		
		saver2.setBalance(3000);
		System.out.println("\n\tID : " + saver2.getID() + "\n\tCurrent Balance : " + saver2.getBalance());
		System.out.println("\tMonthly Interest : " + saver2.calculateMonthlyInterest());
		System.out.println("\tNext months balance : " + saver2.savingsBalance());



	}
}