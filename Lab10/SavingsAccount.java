
public class SavingsAccount
{
    private int accNum;
    static int idnum = 1;
    private double balance;
    private double savingsBalance;
    private static double annualInterestRate;
    private double interest;
	
	public SavingsAccount(double balance, double annualInterestRate)			// constructor method #1
	{
		this.balance = balance;
		this.annualInterestRate = annualInterestRate;
		accNum = idnum;
		idnum++;
	}
	
	public String toString()
	{
		String ret = "\n\tID : " + getID() + "\n\tCurrent Balance : " + getBalance() + "\n\tMonthly Interest : " + calculateMonthlyInterest() + "\n\tNext Months Balance : " + savingsBalance();
		
		return ret;
	}
	
	public void modifyInterestRate(double z)
	{
		annualInterestRate = z;
	}
		
	
	public int getID()
	{
		return accNum;
	}
	
	
	public void setBalance(double x)
	{
		balance = x;		
	}
	
	public double getBalance()
	{
		return balance;
	}
	
	public double savingsBalance()
	{
		savingsBalance = balance + interest;
		return savingsBalance;
	}
	
	public double calculateMonthlyInterest()
	{
		interest = balance * annualInterestRate / 12;
		
		return interest;
	}
	
}