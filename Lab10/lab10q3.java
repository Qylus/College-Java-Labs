 import java.util.Scanner;

public class lab10q3
{
	public static void main (String[] args) throws InterruptedException
	{		
		Scanner s = new Scanner(System.in);
		
		int x = 0, countA = 0;
		
		BankCustomer ledgerA = new BankCustomer("Guy Manderson", "House on, the Street");
		SavingsAccount saverA = new SavingsAccount(5000, 20);
		SavingsAccount saverB = new SavingsAccount(3000, 15);
		SavingsAccount saverC = new SavingsAccount(13000, 30);

		do 
		{
			System.out.print("\n\t1. Add Savings Account\n\t2. Return Total Savings\n\t3. Return Summary of Accounts\n\t4. Exit\n\n\tEnter number : ");
			x = s.nextInt();
			
			switch (x)
				{
					case 1 : if (countA == 0)
							 	{
							 		
							 		ledgerA.addAccount(saverA);
							 		System.out.print("\n\n\t");
							 	}
					
							 else if (countA == 1)
								 {
							 		ledgerA.addAccount(saverB);
							 		System.out.print("\n\n\t");
								 }
							 	
							 	
							 else if (countA == 2)
								 {
									ledgerA.addAccount(saverC);
							 		System.out.print("\n\n\t");
								 }
								 
							 else 
							 	System.out.print("\n\tYou have reached the maximum number of savings accounts.\n\n\t");

							 	countA++;
							 	
							 	break;
							 	
							 	
					case 2 : if (countA == 0)
								System.out.print("\n\tNo Savings Accounts found.\n\t");
								
							 else if (countA == 1)
								System.out.print("\n\tBalance of ledger A is " + ledgerA.balance(saverA) + ".\n\t");
								
							 else if (countA == 2)
								System.out.print("\n\tBalance of ledger A is " + (ledgerA.balance(saverA) + ledgerA.balance(saverB)) + ".\n\t");
									
							 else
								System.out.print("\n\tBalance of ledger A is " + (ledgerA.balance(saverA) + ledgerA.balance(saverB) + ledgerA.balance(saverC)) + ".\n\t");
							
							 	break;
							 	
							 	
					case 3 : if (countA == 0)
								System.out.print("\n\tNo Savings Accounts found.\n\t");
								
							 else if (countA == 1)
								System.out.print("\n\t" + ledgerA.summary(saverA) + ".\n\t");
								
							 else if (countA == 2)
								System.out.print("\n\t" + ledgerA.summary(saverA) + ledgerA.summary(saverB) + ".\n\t");
									
							 else
								System.out.print("\n\t" + ledgerA.summary(saverA) + ledgerA.summary(saverB) + ledgerA.summary(saverC) + ".\n\t");
;
							 	break;
							 	
							 	
					case 4 : System.out.print("\n\tThank you for your patronage.\n\t");
							 
				}	
		}
		while (x != 4);
		
	}
}