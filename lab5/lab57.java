import java.util.Scanner;

class lab57
{
	public static void main (String[] args)
	{
		Scanner keyboard = new Scanner (System.in);
		
		int input;
		System.out.print("Enter the temperature : ");
		input = keyboard.nextInt();
		
		if (input > -11 && input < 6)
			System.out.println("It is very cold ");
			
		else if (input < 16)
			System.out.println("It is cold ");
			
		else if (input < 22)
			System.out.println("It is warm ");
			
		else if (input < 31)
			System.out.println("It is hot ");
		
		else
			System.out.println("Error, temperature out of range");
	}
	
}