
class lab54
{
	public static void main (String[] args)
	{
		int i, square, cube;
		
		System.out.println("Number\tSquare\tCube\n");
		
		for (i = 1; i <= 5; i++)
			{
				square = i * i;
				cube = i * i * i;
				System.out.println(i + "\t" + square + "\t" + cube + "\n");
				square = 0;
				cube = 0;
			}
	}
}