import java.util.Scanner;

class lab59
{
	public static void main (String[] args)
	{
		int i, input, x = 0;
		Scanner keyboard = new Scanner (System.in);
		
		for (i = 0; i < 10; i++)
			{
				System.out.print("Enter number " + (i + 1) + " : ");
				input = keyboard.nextInt();
				
				if (i == 0)
					x = input;
				
				if (input > x)
					x = input;
			}	
			
		System.out.println("The biggest number was " + x);
	}

}