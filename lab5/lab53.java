import java.util.Scanner;

class lab53
{
	public static void main (String[] args)
	{
		Scanner keyboard = new Scanner (System.in);
		
		int input, i, sum = 0;
		
		System.out.print("Please enter input value : ");
		input = keyboard.nextInt();
		
		if (input < 2)
			{
				System.out.print("Error : Input less than 2. Exiting.\n"); //If input is less than 2, exits program
				System.exit(0);
			}
		
		for (i = 2; i <= input; i++)
			sum = i + sum; // starting at 2, increment number towards input. For each time the loop runs, add the current index value to the sum.
		
		
		System.out.println("The sume of the numbers 2 through "+ input + " is " + sum);
	}
}