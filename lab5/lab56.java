import java.util.Scanner;

class lab56
{
	public static void main (String[] args)
	{
		Scanner keyboard = new Scanner (System.in);
		
		int input;
		System.out.print("Enter your age : ");
		input = keyboard.nextInt();
		
		if ( input < 18)
			System.out.println("You are not old enough to vote ");
		
		else
			System.out.println("You are old enough to vote");
	}
}