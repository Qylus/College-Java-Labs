/*
  __________________________________________________
 |"The Quickest and Dirtiest"                       |
 |Courtesy of Kyle Howard, c00207060                |
 |03/04/2017 ||| 04:07 ||| Requesting more coffee   |
 |                                                  |
 |                             1997 ~ 2017          |
 |                              Hurt me Plenty      |
 |__________________________________________________|

                  __            /^\
                .'  \          / :.\
               /     \         | :: \
              /   /.  \       / ::: |
             |    |::. \     / :::'/
             |   / \::. |   / :::'/
             `--`   \'  `~~~ ':'/`
                     /         (
                    /   0 _ 0   \
                  \/     \_/     \/
                -== '.'   |   '.' ==-
                  /\    '-^-'    /\
                    \   _   _   /
                   .-`-((\o/))-`-.
              _   /     //^\\     \   _
            ."o".(    , .:::. ,    )."o".
            |o  o\\    \:::::/    //o  o|
             \    \\   |:::::|   //    /
              \    \\__/:::::\__//    /
               \ .:.\  `':::'`  /.:. /
                \':: |_       _| ::'/
                 `---` `"""""` `---`
                 Happy         Easter
                 -----         ------
 */

import java.awt.event.*;
import java.awt.Color;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

public class Calculator implements ActionListener {

    private String memory; //Stores value for manipulation with the calculators memory buttons
    private static double num1 = 0, num2 = 0, sum = 0; //These doubles are used to perform calculations
    private  static int operator = 0; //This int is used to keep track of what operand button was pressed

    private Calculator()
    {
        Border border = new LineBorder(Color.BLACK, 1); //Defining border parameters
        JFrame frame = new JFrame("Calculator"); //Who doesn't love a title on their frame
        //Little amounts of style used to preserve minimalist aesthetic

        /*
        Attempt at more efficient/elegant button deceleration. I unfortunately couldn't solve this problem in time, thus the large blocs of repetitious code remain.

        String faces[] = new String[]{"b1", "b2", "b3", "b4", "b5", "b6", "b7", "b8", "b9", "b0", "bDec", "bClear", "bAdd", "bSub", "bMul", "bDiv", "bEq", "memS", "memC", "memR", "bInvis"};
        String plates[] = new String[]{"7", "8", "9", "4", "5", "6", "1", "2", "3", "0", ".", "C", "/", "*", "-", "+", "=", "MS", "MC", "MR", ""};

        for (int i = 0; i < faces.length; i++)
        {
            JButton("" + String.valueOf(faces[i])) = new JButton("" + plates[i]);
            buttons[i] = new JButton(String.valueOf(faces[i]));
        } */

        //Button declaration
        JButton b1 = new JButton("7");
        JButton b2 = new JButton("8");
        JButton b3 = new JButton("9");
        JButton b4 = new JButton("4");
        JButton b5 = new JButton("5");
        JButton b6 = new JButton("6");
        JButton b7 = new JButton("1");
        JButton b8 = new JButton("2");
        JButton b9 = new JButton("3");
        JButton bDec = new JButton(".");
        JButton b0 = new JButton("0");
        JButton bClear = new JButton("C");
        JButton bDiv = new JButton("/");
        JButton bMul = new JButton("*");
        JButton bSub = new JButton("-");
        JButton bAdd = new JButton("+");
        JButton bEq = new JButton("=");
        JButton memS = new JButton("MS");
        JButton memC = new JButton("MC");
        JButton memR = new JButton("MR");

        //Glitch causes final button in frame to appear invisible, so extra button is added to counteract this
        //Shoutout to Sean Murphy for this workaround
        JButton ghost = new JButton("");
        ghost.setVisible(false); //Sets extra button to be invisible to retain uniform layout

        JTextField inputField = new JTextField(); //Initialising text field
        inputField.setBounds(25,30,170,50); //Bounderies of text field
        inputField.setEditable(false); // Disable editing of text field, inputs can only be taken from panel, reducing human error
        inputField.setHorizontalAlignment(SwingConstants.RIGHT); //Justifies the entered text to the right
        frame.add(inputField);

        //Assigning XY Coordinates and sizes to button
        b1.setBounds(30,85,25,25);
        b2.setBounds(65,85,25,25);
        b3.setBounds(100,85,25,25);
        b4.setBounds(30,125,25,25);
        b5.setBounds(65,125,25,25);
        b6.setBounds(100,125,25,25);
        b7.setBounds(30,165,25,25);
        b8.setBounds(65,165,25,25);
        b9.setBounds(100,165,25,25);
        b0.setBounds(65,205,25,25);
        bDec.setBounds(30,205,25,25);
        bClear.setBounds(100,205,25,25);
        bDiv.setBounds(165,85,25,25);
        bMul.setBounds(165,123,25,25);
        bSub.setBounds(165,165,25,25);
        bAdd.setBounds(165,205,25,25);
        memS.setBounds(30,245,25,25);
        memC.setBounds(65,245,25,25);
        memR.setBounds(100,245,25,25);
        bEq.setBounds(165,245,25,25);
        ghost.setBounds(200,400,25,25);

        /*
        Another failed attempt at optimization.
        had this loop worked, nearly 50 lines of upcoming code would be redundant

        for (int i = 0; i <= faces.length; i++)
        {
            frame.add(buttons[i]);
            buttons[i].setBorder(border)
        }*/

        //Adding all buttons to the frame
        frame.add(b1);
        frame.add(b2);
        frame.add(b3);
        frame.add(b4);
        frame.add(b5);
        frame.add(b6);
        frame.add(b7);
        frame.add(b8);
        frame.add(b9);
        frame.add(bDec);
        frame.add(b0);
        frame.add(bClear);
        frame.add(bAdd);
        frame.add(bSub);
        frame.add(bMul);
        frame.add(bDiv);
        frame.add(memS);
        frame.add(memC);
        frame.add(memR);
        frame.add(bEq);
        frame.add(ghost);

        //Assigning border to all buttons
        b1.setBorder(border);
        b2.setBorder(border);
        b3.setBorder(border);
        b4.setBorder(border);
        b5.setBorder(border);
        b6.setBorder(border);
        b7.setBorder(border);
        b8.setBorder(border);
        b9.setBorder(border);
        bDec.setBorder(border);
        b0.setBorder(border);
        bClear.setBorder(border);
        bAdd.setBorder(border);
        bSub.setBorder(border);
        bMul.setBorder(border);
        bDiv.setBorder(border);
        memS.setBorder(border);
        memC.setBorder(border);
        memR.setBorder(border);
        bEq.setBorder(border);

        frame.setVisible(true); //making the frame visible
        frame.setResizable(false); //Making frame unresizable
        frame.setLayout(null); //Null layout allows for the use of the setBounds method, which is more versatile
        frame.setSize(225,325); //Setting frame size
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE); //Allows the frame to be closed by the user


        //Implementing action listeners to buttons.
        //The action listeners for numbers and the decimal simply concatenate the character to the right of the current content in the text field.
        //Again, I feel this could have been achieved more efficiently in a loop, but I could not figure out proper implementation of this.

        //("Something something lambda expression" - IntelliJ, 2017. This message repeats.)\\
        
        b1.addActionListener(new ActionListener()
                             {
                                 public void actionPerformed(ActionEvent e)
                                 {
                                     inputField.setText(inputField.getText().concat("7"));
                                 }
                             }
        );
        b2.addActionListener(new ActionListener()
                             {
                                 public void actionPerformed(ActionEvent e)
                                 {
                                     inputField.setText(inputField.getText().concat("8"));
                                 }
                             }
        );
        b3.addActionListener(new ActionListener()
                             {
                                 public void actionPerformed(ActionEvent e)
                                 {
                                     inputField.setText(inputField.getText().concat("9"));
                                 }
                             }
        );
        b4.addActionListener(new ActionListener()
                             {
                                 public void actionPerformed(ActionEvent e)
                                 {
                                     inputField.setText(inputField.getText().concat("4"));
                                 }
                             }
        );
        b5.addActionListener(new ActionListener()
                             {
                                 public void actionPerformed(ActionEvent e)
                                 {
                                     inputField.setText(inputField.getText().concat("5"));
                                 }
                             }
        );
        b6.addActionListener(new ActionListener()
                             {
                                 public void actionPerformed(ActionEvent e)
                                 {
                                     inputField.setText(inputField.getText().concat("6"));
                                 }
                             }
        );
        b7.addActionListener(new ActionListener()
                             {
                                 public void actionPerformed(ActionEvent e)
                                 {
                                     inputField.setText(inputField.getText().concat("1"));
                                 }
                             }
        );
        b8.addActionListener(new ActionListener()
                             {
                                 public void actionPerformed(ActionEvent e)
                                 {
                                     inputField.setText(inputField.getText().concat("2"));
                                 }
                             }
        );
        b9.addActionListener(new ActionListener()
                             {
                                 public void actionPerformed(ActionEvent e)
                                 {
                                     inputField.setText(inputField.getText().concat("3"));
                                 }
                             }
        );
        bDec.addActionListener(new ActionListener()
                               {
                                   public void actionPerformed(ActionEvent e)
                                   {
                                   	
                                   		boolean dec = false;
                                   		int i;
                                   		
                                   		for (i = 0; i < inputField.getText().length(); i++) //Error handling multiple decimal point entries
                                   		{
                                   			if (inputField.getText().charAt(i) == '.')
                                   			{
                                   				dec = true;
                                   			}
                                   		
                                   		}
                                   		
                                   		if (dec == false)
                                   		{
                                   			inputField.setText(inputField.getText().concat("."));
                                   		}
                                       
                                   }
                               }
        );
        b0.addActionListener(new ActionListener()
                             {
                                 public void actionPerformed(ActionEvent e)
                                 {
                                     inputField.setText(inputField.getText().concat("0"));
                                 }
                             }
        );

        bClear.addActionListener(new ActionListener()
                                 {
                                     public void actionPerformed(ActionEvent e)
                                     {
                                         inputField.setText(""); //If clear button is pressed, text field is set to empty String
                                     }
                                 }
        );

        //How the calculator works logically :
        //1. Number is entered into text field
        //2. When operator is pressed, the current number is stored
        //3. The value of the operator is stored
        //4. Number is entered into text field, or the result of the previous equation is used
        //5a. Equals is pressed || 5b. Operator is pressed
        //6a. Current number is stored, and the corresponding operation, as defined by the operator value, is performed on the two stored numbers || 6b. Another number is entered
        //7a. The solution is printed to the screen || 7b. This can continue indefinitely. When equals is pressed, the calculator will take into account the last two numbers, and the operator separating them when solving


        bAdd.addActionListener(new ActionListener()
                               {
                                   public void actionPerformed(ActionEvent e)
                                   {
                                       num1 = Double.parseDouble(inputField.getText()); //Gets and stores the number inputted before the operator button was pressed
                                       operator = 1; //Setting operator to be used when calculating
                                       inputField.setText(""); //This is repeated below
                                   }
                               }
        );
        bSub.addActionListener(new ActionListener()
                               {
                                   public void actionPerformed(ActionEvent e)
                                   {
                                       num1 = Double.parseDouble(inputField.getText());
                                       operator=2;
                                       inputField.setText("");
                                   }
                               }
        );
        bMul.addActionListener(new ActionListener()
                               {
                                   public void actionPerformed(ActionEvent e)
                                   {
                                       num1 = Double.parseDouble(inputField.getText());
                                       operator=3;
                                       inputField.setText("");
                                   }
                               }
        );
        bDiv.addActionListener(new ActionListener()
                               {
                                   public void actionPerformed(ActionEvent e)
                                   {
                                       num1 = Double.parseDouble(inputField.getText());
                                       if(num1 == 0)
                                       {
                                         inputField.setText("Error: Cannot divide by zero"); //Error handling divison by zero
                                       }
                                       else
                                       {
                                        operator = 4;
                                        inputField.setText("");
                                       }

                                   }
                               }
        );
        memS.addActionListener(new ActionListener()
                               {
                                   public void actionPerformed(ActionEvent e)
                                   {
                                       memory = inputField.getText(); //Stores what is currently in the text field in the memory variable
                                   }
                               }
        );

        memC.addActionListener(new ActionListener()
                               {
                                   public void actionPerformed(ActionEvent e)
                                   {
                                       memory = ""; //Overwrites memory to nothing, clearing it
                                   }
                               }
        );

        memR.addActionListener(new ActionListener()
                               {
                                   public void actionPerformed(ActionEvent e)
                                   {
                                       inputField.setText(memory); //Sets the text field to the contents of the memory variable
                                   }
                               }
        );
        bEq.addActionListener(new ActionListener()
                              {
                                  public void actionPerformed(ActionEvent e)
                                  {
                                      num2 = Double.parseDouble(inputField.getText()); //When equals is pressed, the number currently in the text field is stored in this variable.

                                      switch(operator) //Each case corresponds to an operand
                                      {
                                          case 1: sum = num1 + num2 ; //Addition
                                              break;
                                          case 2: sum = num1 - num2 ; //Subtraction
                                              break;
                                          case 3: sum = num1 * num2 ; //Multiplication
                                              break;
                                          case 4: if (num2 == 0){
			                                      	inputField.setText("Error: Cannot divide by zero");
			                                      	break;
			                                      	}
			                                      	
			                                      else{
			                                      	sum = num1 / num2 ; //Division		
			                                      	}
                                          		  	
                                              break;

                                          default: sum = 0;
                                      }
                                      inputField.setText("" + sum); //When the equation is complete, display the answer in the text field
                                  }
                              }
        );
    }

    // Main method
    public static void main(String[] args)
    {
        new Calculator();
    }

	// Method overriding
    @Override
    public void actionPerformed(ActionEvent e) {

    }
}