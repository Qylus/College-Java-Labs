/*
** Author: KH	Date: September 2015
** Demonstrates the basic structure of a Java application
*/
public class Hello
{
	// prints a simple message�
	public static void main(String args[])
	{
		System.out.print("Hello, I�m a Java program\n");
	}
}

/*Error 1 : "Change the first Hello to hello"
	    Class designation is case sensitive

Error 2 : "Change Java to JAVA"
	  No error, contents of string did not effect code syntax

Error 3 : "Insert your own initials in place of GD"
	  No error, change made in commented line

Error 4 : "Remove the first quotation mark in the string literal"
	  Invalid syntax

Error 5 : "Change main to min"
	  Invalid syntax

Error 6 : "Change println to printline"
	  Invalid syntax

Error 7 : "Remove the semicolon at the end of the println statement"
	  Invalid syntax

Error 8 : "Remove the first brace ( { ) in the program."
	  Error '{' expected				*/
