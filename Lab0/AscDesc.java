import java.io.*;
// Author: KH	Date: September 2016 
class AscDesc
{
	public static void main(String args[]) throws IOException
  	{
		String p1;         
		int i = 0;
  
    		BufferedReader keyboardInput;
    		// std. i/p is keyboard for console apps and buffered I/O
    		keyboardInput = new BufferedReader(new InputStreamReader(System.in));
    		p1 = new String("X");

    
    		System.out.print("Enter an 'a' for ascending or 'd' for descending: ");
    		p1  = keyboardInput.readLine();
    		System.out.println();                    // new line
    
    		if (p1 == "a")
		{
			for (i = 0; i < 100; i++)
			System.out.println(i);
		}

		else if (p1 == "d")
		{
			for (i = 100; i > 0; i--)
			System.out.println(i);
		}

		else
		System.out.print("Invalid Input");
  	}
}
