import java.util.Scanner;

class lab62
{
	public static void main (String[] args)
	{
		int i, input = 0;

		Scanner keyboard = new Scanner (System.in);
		
		while (input > -1)
		{
			System.out.print("Number of stars? (Any negative number to quit) : ");
			input = keyboard.nextInt();
			
			System.out.print("\n");


			for (i = 0; i < input; i++)
				{
					System.out.print("*");
				}
				
			System.out.print("\n\n");
		}
		
		System.out.println();
		
			
	}

}