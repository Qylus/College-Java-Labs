import java.util.Scanner;

class lab65
{
	public static void main (String[] args)
	{
		int i, productNum = 1;
		int[] quantityArray;
		double[] retailPri;
		
		quantityArray = new int[5];
		retailPri = new double[5];

		Scanner keyboard = new Scanner (System.in);
		
		System.out.print("There are 5 products available.\n(Input 0 when finished)");

			
		while (productNum != 0)
		
		{
			System.out.print("\nEnter product number : ");
			productNum = keyboard.nextInt();
				
			if (productNum > 6 || productNum < 0)
				System.out.println("Invalid Input");
				
			switch (productNum) //This switch statement gets the retail price by multiplying the product quantity  by the hardcoded price per item
				{				//I used arrays for the sake of brevity
		            case 1:  System.out.print("Please enter quantity of product 1 : ");
		            			quantityArray[0] = keyboard.nextInt(); //Stores quantity of specific product in array
		            			retailPri[0] = quantityArray[0] * 2.98;	//Calculates retail price and stores this in an array, also
		                     break;
		            case 2:  System.out.print("Please enter quantity of product 2 : ");
		                        quantityArray[1] = keyboard.nextInt();
		                        retailPri[1] = quantityArray[1] * 4.50;
		                     break;
		            case 3:  System.out.print("Please enter quantity of product 3 : ");
		                        quantityArray[2] = keyboard.nextInt();
		                        retailPri[2] = quantityArray[2] * 9.98;
		                     break;
		            case 4:  System.out.print("Please enter quantity of product 4 : ");
		                        quantityArray[3] = keyboard.nextInt();
		                        retailPri[3] = quantityArray[3] * 4.49;
		                     break;
		            case 5:  System.out.print("Please enter quantity of product 5 : ");
		                       	quantityArray[4] = keyboard.nextInt();
		                        retailPri[4] = quantityArray[4] * 6.87;
		                     break;
		        }
		}
		
		for (i = 0; i < retailPri.length; i++) //Prints a line of output for each loop iteration, which also  corresponds to array positions
			System.out.println(quantityArray[i] + " product " + (i + 1) + "'s were found. Total price = " + Math.round(retailPri[i] * 10000.0) / 10000.0 + " euro");
			
		System.out.println();
							
		keyboard.close();				
	}

}