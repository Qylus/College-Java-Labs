import java.util.Scanner;


class lab67
{
	public static void main (String[] args)
	{
		Scanner keyboard = new Scanner (System.in);
				
		int i, input = 0, math1, math2;
		
		while (input != -1) // while the input is not the centinal value, loop
			{
				math1 = (int )(Math.random() * 12 + 0); //assigns two variables to two random whole numbers ranging from 0 to 12
				math2 = (int )(Math.random() * 12 + 0); // these are reasigned for each loop iteration
			
				System.out.print("(Input -1 to quit)\nWhat is " + math1 + " x " + math2 + "? : ");
				input = keyboard.nextInt(); // input get
				
				if (input == -1)
					System.out.println("Goodbye\n"); //centinal value
	
				else if (input == (math1 * math2))	//checks user input against the the multiplied value of the two random integers
					System.out.println("Correct\n"); //if theyre the same, the answer was correct
					
				else
					System.out.println("Incorrect\n"); //if not, the answer was incorrect
			}				
	}
}