import java.util.Scanner;

class lab64
{
	public static void main (String[] args)
	{
		int i, i2, input;

		Scanner keyboard = new Scanner (System.in);
		
		System.out.print("Enter number between 1 and 10 : ");
		input = keyboard.nextInt();

		for (i = 0; i < input; i++)	
		{
			for (i2 = input; i2 > i; i2--)
				System.out.print("*");
			
			System.out.println();	
		}				
	}

}