class lab66
{
	public static void main (String[] args)
	{
		int i, x, y = 1;
		
		for (i = 0; i < 12; i++)
			{
				for (x = 0; x <= 12; x++)
					System.out.println(y + " x " + x + " = " + (y * x)); //for each iteration of the first loop, multiply an incrementing number by its current index value
				
				System.out.println("\n"); //Newline after each table for readability 
				
				y++; //incrementing value to multiply by in the second loop
			}
	}
			
			
} 

/*class lab66{
	public static void main (String[] args){
		int i, x, y = 1;
		for (i = 0; i < 12; i++){ //first loop runs 12 iterations
			for (x = 0; x <= 12; x++)
				System.out.println(y + " x " + x + " = " + (y * x)); //for each iteration of the first loop, multiply an incrementing number by its current index value
			
			System.out.println("\n"); //linebreak inbetween iterations of the first loop
			y++;}}} //incrementing value to multiply by in the second loop
			
			Just wanted to see how short the program could be made if formatting conventions were ignored.
*/