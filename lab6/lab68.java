import java.util.Scanner;

class lab68
{
	public static void main (String[] args)
	{
		Scanner keyboard = new Scanner (System.in);
				
		int i, i2, count = 0, input;
		
		System.out.print("Please input an integer (greater than 1) : ");
		input = keyboard.nextInt();
		
		for (i = 1; i <= input; i+=2)
		{
			if (i < 2)
				System.out.println(2);
				
			count = 0;
			
			for (i2 = i; i2 >= 1; i2--)
				if (i % i2 == 0)
					count++;
			
			if (count == 2)	
				System.out.println(i);
		}	
	}
}
