public class Employee extends Person
{
	private String name;
	private double salary;
	
	public Employee(String name, double salary)
  	{ 
  		super(name);
  		this.salary = salary;
  	} 
  	
  	public String getDescription()
  	{
  		String s = "An employee with a salary of " + salary + "\n";
  		
  		return s;
  	}
  	
}