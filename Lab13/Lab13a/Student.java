public class Student extends Person
{
	private String name;
	private String course;
	
	public Student(String name, String course)
  	{ 
  		super(name);
  		this.course = course;
  	} 
  	
  	public String getDescription()
  	{
  		String s = "A student studying " + course + "\n";
  		
  		return s;
  	}
  	
}