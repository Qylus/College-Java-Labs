public class CD extends LibraryItem implements LoanItem
{
	String band, title;
	int numTracks;
	
	public CD(String type, String ID, String band, String title, int numTracks)
	{
		super(type, ID);
		this.band = band;
		this.title = title;
		this.numTracks = numTracks;
	}
	
	public void calculatePrice()
	{
		System.out.println("\n\tCD price has been calculated.");
	}
}