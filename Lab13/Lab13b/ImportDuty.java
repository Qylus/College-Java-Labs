public interface ImportDuty
{
	double CARTAXRATE = 0.10;
	double HGVTAXRATE = 0.15;
	
	public void calculateDuty();
}