class bQ2Drive
{
	public static void main (String[] args)
	{
		Car car1 = new Car("Honda", 4, 4);
		Hgv hgv1 = new Hgv(6, 7, 8);
		
		RoadVehicle[] itemArr = new RoadVehicle[2];
		itemArr[0] = car1;
		itemArr[1] = hgv1;
		
		car1.calculateDuty();
		hgv1.calculateDuty();
	}
}