class bQ1Drive
{
	public static void main (String[] args)
	{
		Book book1 = new Book("Fantasy", "QE204O", "Krill Waverhand", "The Capricious Compendium of the Dubiously Esoteric", 3412);
		CD cd1 = new CD("Jazz", "FG817P", "The Carseats", "Waving into Ethers Door", 14);
		
		LibraryItem[] itemArr = new LibraryItem[2];
		itemArr[0] = book1;
		itemArr[1] = cd1;
		
		book1.calculatePrice();
		cd1.calculatePrice();
	}
}