public class Car extends RoadVehicle implements ImportDuty
{
	private String carType;

	public Car() {
	 	this("", 0, 0); }

	public Car(String c, int w, int p) { 
		super(w, p);
		setType(c);
	}

	public void setType(String t)  {
		carType = t;
	}

	public String getType() {
		return carType;
	}
	
	public void calculateDuty()
	{
		double tax = 5000 * CARTAXRATE;
		double finaltax = tax + 5000;
		
		System.out.println("\n\tCar Amount : 5000\n\tCar Amount after tax : " + finaltax);
	}
}