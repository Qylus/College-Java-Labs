public class Hgv extends RoadVehicle implements ImportDuty
{
	private int cargo;

	public Hgv()
	{ 	this(0,0,0);	}

	public Hgv(int c, int w, int p)
	{ 
		super(w, p);
		setCargo(c);
	}

	public void setCargo(int size)
	{
		cargo = size;
	}

	public int getCargo()
	{
		return cargo;
	}	
	
	public void calculateDuty()
	{
		double tax = 5000 * HGVTAXRATE;
		double finaltax = tax + 5000;
		
		System.out.println("\n\tHGV Amount : 5000\n\tHGV Amount after tax : " + finaltax);
	}
}
